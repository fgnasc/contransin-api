import './config/env';
import './config/database';
import routes from './server/routes';
import Server from './server/index';

const port = parseInt(process.env.PORT);
export default new Server().router(routes).listen(port);
