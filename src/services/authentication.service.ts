import { UserInstance } from '../interfaces';
import { User } from '../models';
import { httpErrors } from '../exceptions';

interface Auth {
  auth: boolean;
  error?: Error;
  user?: UserInstance;
  token?: string;
}

export class AuthenticationService {
  async login(username: string, password: string, remember: boolean): Promise<Auth> {
    let user = await User.authenticate(username, password);
    
    if (!user) throw new Error('Usuário não encontrado');
    const token: any = await user.generateAuthToken(remember);
    user = user.toObject();
    delete user.password;
    return { auth: true, user, token };
  }

  // async logout(id: string, token: string): Promise<Object> {
  //   const user = await User.findOne({ _id: id });
  //   try {
  //     user.tokens = user.tokens.filter(uToken => uToken.token !== token);
  //     await user.save();
  //     return { status: 200 };
  //   } catch (error) {
  //     return httpErrors(500);
  //   }
  // }

  async logoutAll(id: string): Promise<Object> {
    const user = await User.findOne({ _id: id });
    try {
      await user.save();
      return { status: 200 };
    } catch (error) {
      return httpErrors(500);
    }
  }
}

export default new AuthenticationService();