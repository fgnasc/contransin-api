import _ from 'lodash';
import { Permission } from '../models';
export class PermissionService {
  constructor() {}

  static async find(query: any) {
    return await Permission.find(query);
  }

  static async create(_props) {
    const permission = await Permission.create(_props);

    if (permission.errors) {
      throw permission.errors;
    }
    return permission;
  }

  static update = async (newProps, instance?) => {
    if (!_.isEmpty(newProps._id)) {
      instance = instance ?? (await Permission.findOne({ _id: newProps._id })) ?? {};
    }
    if (_.isEmpty(instance)) throw new Error('Fail to save permission');

    for (let prop in newProps) {
      instance[prop] = newProps[prop];
    }
    return instance.save();
  };
}

export default new PermissionService();
