import nodemailer from 'nodemailer';

export class Service {
  transporter = undefined;

  constructor(args) {
    this.create(args);
  }

  create(options) {
    try {
      const _defaults = {
        host: process.env.SMTP_HOST,
        port: process.env.SMTP_PORT,
        // secure: true,
        auth: {
          user: process.env.SMTP_USER,
          pass: process.env.SMTP_PASS,
        },
        tls: { rejectUnauthorized: false },
      };
      if (!options) options = {};
      const params = { ..._defaults, ...options };
      this.setTransporter(nodemailer.createTransport(params));
    } catch (error) {
      throw new Error(error);
    }
  }

  async send(options?, callback?) {
    try {
      const _default = {
        from: process.env.SMTP_USER2,
        replyTo: process.env.SMTP_REPLY,
        to: process.env.SMTP_DEFAULT_TO,
      };
      const mailOptions = { ..._default, ...options };
      return await this.getTransporter().sendMail(mailOptions);
    } catch (error) {
      throw new Error('Falha ao enviar email');
    }
  }

  setTransporter(transporter) {
    this.transporter = transporter;
  }

  getTransporter() {
    return this.transporter;
  }
}

export default new Service(null);