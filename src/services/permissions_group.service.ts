import _ from 'lodash';
import { PermissionsGroup } from '../models';
export class PermissionsGroupService {
  constructor() {}

  static async find(query: any) {
    return await PermissionsGroup.find(query);
  }

  static async create(_props) {
    const permissionsGroup = await PermissionsGroup.create(_props);

    if (permissionsGroup.errors) {
      throw permissionsGroup.errors;
    }
    return permissionsGroup;
  }

  static update = async (newProps, instance?) => {
    if (!_.isEmpty(newProps._id)) {
      instance = instance ?? (await PermissionsGroup.findOne({ _id: newProps._id })) ?? {};
    }
    if (_.isEmpty(instance)) throw new Error('Fail to save permission');

    for (let prop in newProps) {
      instance[prop] = newProps[prop];
    }
    return instance.save();
  };
}

export default new PermissionsGroupService();
