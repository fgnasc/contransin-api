import _ from 'lodash';
import { Group } from '../models';
export class GroupService {
  constructor() {}

  static async find(query: any) {
    return await Group.find(query);
  }

  static async create(_props) {
    const group = await Group.create(_props);
    if (group.errors) {
      throw group.errors;
    }
    return group;
  }

  static update = async (newProps, instance?) => {
    if (!_.isEmpty(newProps._id)) {
      instance = instance ?? (await Group.findOne({ _id: newProps._id })) ?? {};
    }
    if (_.isEmpty(instance)) throw new Error('Fail to save group');

    for (let prop in newProps) {
      instance[prop] = newProps[prop];
    }
    return instance.save();
  };

}

export default new GroupService();
