import _ from 'lodash';
import bcrypt from 'bcryptjs';
import { Types } from 'mongoose';
import { User, User as Users } from '../models';
import MailService from './mail.service';
import fs from 'fs';
const ObjectId = Types.ObjectId;
export class UserService {
  constructor() {}

  static async findById(query: any) {
    return await Users.find(query).select('-password');
  }

  static async create(_props) {
    const user = await User.create(_props);
    if (user.errors) {
      throw user.errors;
    }
    return user
  }

  static update = async (newProps, instance?) => {
    if (newProps.email) {
      newProps.username = newProps.email;
    }

    if (!_.isEmpty(newProps._id)) {
      instance = instance ?? (await Users.findOne({ _id: newProps._id })) ?? {};
    }
    if (_.isEmpty(instance)) throw new Error('Fail to save user');

    for (let prop in newProps) {
      instance[prop] = newProps[prop];
    }
    return instance.save();
  };

  static async updateProfilePicById(_id: string, profilePicPath: string) {
    let profile_pic;
    try {
      profile_pic = (await Users.findById(_id).select('profile_pic')).profile_pic;
      if (profile_pic === profilePicPath) {
        return null;
      }
      await Users.update({ _id }, { profile_pic: profilePicPath });
    } catch (error) {
      throw error;
    }

    try {
      fs.unlinkSync(profile_pic);
    } catch (error) {
      throw error;
    }
    return profilePicPath;
  }

  static async changePassword(_id: string, password: any) {
    const result = await Users.findById(_id);
    const match = await bcrypt.compare(password.actualPassword, result.password);
    if (!match) return Error('Erro');
    await Users.update({ _id }, { password: password.password });
  }

  static async recover(email: string) {
    const instance = await Users.findOne({email});
    if (!instance) throw new Error('Usuário não encontrado');
    const newPassword = Math.random().toString(36).slice(-8);
    instance.password = newPassword;
    await instance.save();
    const response = await MailService.send({
      to: instance.email,
      subject: 'Recuperação de senha',
      html: `Olá, foi solicitado a redefinição da sua senha`,
    });
    return response;
  }
}

export default new UserService();