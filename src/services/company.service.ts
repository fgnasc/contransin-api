import _ from 'lodash';
import { Company, User } from '../models';
import fs from 'fs';
export class CompanyService {
  constructor() {}

  static async find(query: any) {
    return await Company.find(query);
  }

  static async create(_props) {
    const company = await Company.create(_props);

    const user: any = {
      company: company._id,
      name: company.name,
      document: company.document,
      username: company.email,
      email: company.email,
      password: 'Contransin@123',
    };

    await User.create(user);
    if (company.errors) {
      throw company.errors;
    }
    return company;
  }

  static update = async (newProps, instance?) => {
    if (!_.isEmpty(newProps._id)) {
      instance = instance ?? (await Company.findOne({ _id: newProps._id })) ?? {};
    }
    if (_.isEmpty(instance)) throw new Error('Fail to save company');

    for (let prop in newProps) {
      instance[prop] = newProps[prop];
    }
    return instance.save();
  };

  static async updateProfilePicById(_id: string, profilePicPath: string) {
    let company_pic;
    try {
      company_pic = (await Company.findById(_id).select('company_pic')).company_pic;
      if (company_pic === profilePicPath) {
        return null;
      }
      await Company.update({ _id }, { company_pic: profilePicPath });
    } catch (error) {
      throw error;
    }

    try {
      fs.unlinkSync(company_pic);
    } catch (error) {
      throw error;
    }
    return profilePicPath;
  }
}

export default new CompanyService();
