export const databaseDate = (date: string) => {
  const dateArray = date.split('/');
  return dateArray.reverse().join('-');
};
