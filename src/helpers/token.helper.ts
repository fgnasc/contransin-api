import { verify } from 'jsonwebtoken';
import { User } from '../models';

export const decodeToken = token => {
  try {
    return verify(token, process.env.JWT_KEY);
  } catch (error) {
    return error
  }
};

export const userFromToken = async (token: string) => {
  const tokenInfo = decodeToken(token);
  if (tokenInfo.id) {
    const user = await User.findOne({ id: tokenInfo.id, 'tokens.token': token });
    if (!user) return null;
    return user;
  }
  return null;
};
