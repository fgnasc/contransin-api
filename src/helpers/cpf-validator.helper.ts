export const isValidCpf = (value: string): boolean => {
  if (/\.|-/gim.test(value)) value = value.replace(/\.|-/gim, '');
  if (value.length === 11) {
    let _sum = 0;
    let _rest = 0;

    if (value === '00000000000') return false;
    for (let i = 1; i <= 9; i++) _sum += parseInt(value.substring(i - 1, i)) * (11 - i);
    _rest = (_sum * 10) % 11;

    if (_rest === 10 || _rest === 11) _rest = 0;
    if (_rest !== parseInt(value.substring(9, 10))) return false;

    _sum = 0;
    for (let i = 1; i <= 10; i++) _sum += parseInt(value.substring(i - 1, i)) * (12 - i);
    _rest = (_sum * 10) % 11;

    if (_rest === 10 || _rest === 11) _rest = 0;
    if (_rest !== parseInt(value.substring(10, 11))) return false;
    return true;
  }
};
