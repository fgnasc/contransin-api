export class HttpException extends Error {
  constructor(public code: number, error: Error | string = '') {
    super(error instanceof Error ? error.message : error);
    this.message = `${getMessageByCode(code)} ${this.message}`;
  }
}

export const getMessageByCode = (code: number) => {
  switch (code) {
    case 400: return 'Solicitação inválida.';
    case 401: return 'Acesso autorizado apenas para usuários autenticados.';
    case 402: return 'Pagamento não realizado. Ops! Você precisa ter um plano ativo para utilizar essa funcionalidade.';
    case 403: return 'Você não tem permissão para acessar essa informação.';
    case 404: return 'Não encontrado.';
    case 405: return 'Método não permitido.';
    case 500: return 'Erro interno no servidor.';
    case 501: return 'Esta função ainda não foi implementada.';
    case 503: return 'Serviço indisponível.';
    default: return 'Erro interno no servidor.';
  }
};

export const httpErrors = (code, message?) => {
  return {
    code, 
    message: `${getMessageByCode(code)} ${message}`.trim() 
  }
};

export default HttpException;
