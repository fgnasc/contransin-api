import { model, Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import _ from 'lodash';

import { PermissionsGroupInstance, PermissionsGroupModel } from '../interfaces';
const ObjectId = Schema.Types.ObjectId;

export const permissionsGroupSchema = new Schema({
  name: { type: String, required: true },
  company: { type: ObjectId, ref: 'Company', required: true },
  permissions: [{ type: ObjectId, active: Boolean, ref: 'Permission', required: true }],
});

permissionsGroupSchema.plugin(timestamps);
permissionsGroupSchema.plugin(require('mongoose-bcrypt'));

export const PermissionsGroup: PermissionsGroupModel = model<PermissionsGroupInstance, PermissionsGroupModel>(
  'PermissionsGroup',
  permissionsGroupSchema
);

export default PermissionsGroup;
