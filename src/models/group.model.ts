import { model, Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import _ from 'lodash';

import { GroupInstance, GroupModel } from '../interfaces';
const ObjectId = Schema.Types.ObjectId;
export const groupSchema = new Schema({
  name: {
    type: String,
    required: 'Nome do grupo é obrigatório',
  },
  permissions_group: {
    type: ObjectId,
    required: 'Permissões para o grupo é obrigatório',
  },
});

groupSchema.plugin(timestamps);
groupSchema.plugin(require('mongoose-bcrypt'));

export const Group: GroupModel = model<GroupInstance, GroupModel>('Group', groupSchema);

export default Group;
