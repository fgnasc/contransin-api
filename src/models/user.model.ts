import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { model, Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import _ from 'lodash';

import { UserInstance, UserModel } from '../interfaces';
import { isValidCpf } from '../helpers';

const ObjectId = Schema.Types.ObjectId;

export const userSchema = new Schema({
  username: {
    type: String,
    lowercase: true,
    required: 'Usuário é obrigatório',
    trim: true,
    validate: {
      validator: (value: string) => value.length > 5,
      message: 'Usuário deve ter no mínimo 5 caracteres.',
    },
  },
  password: {
    type: String,
    required: 'Senha é obrigatória',
    trim: true,
    bcrypt: true,
    validate: {
      validator: (value: string) =>
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/g.test(value) || // eslint-disable-line
        /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})/g.test(value), // eslint-disable-line
      message: 'A senha não é segura o suficiente.',
    },
  },
  profile_pic: {
    type: String,
    trim: true,
    default: null,
  },
  phone: {
    type: Number,
  },
  name: {
    type: String,
    required: true,
    trim: true,
    validate: {
      validator: (value: string) => value.length > 3,
      message: 'Usuário deve ter no mínimo 3 caracteres.',
    },
  },
  gender: {
    type: String,
    enum: ['MASC', 'FEM', 'OTHER'],
  },
  document: {
    type: String,
    unique: true,
    required: 'CPF é obrigatório',
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
  },
  address: {
    address: {
      type: String,
      default: null,
    },
    number: {
      type: String,
      default: null,
    },
    complement: {
      type: String,
      default: null,
    },
    neighborhood: {
      type: String,
      default: null,
    },
    city: {
      type: String,
      default: null,
    },
    state: {
      type: String,
      default: null,
    },
    zipcode: {
      type: String,
      default: null,
    },
    country: {
      type: String,
      default: 'Brasil',
    },
  },
  active: {
    type: Boolean,
    default: true,
  },
  birth: {
    type: Date,
  },
  company: {
    type: ObjectId,
    required: true,
    ref: 'Company',
  },
  group: {
    type: ObjectId,
    default: null,
    ref: 'Group'
  }
});

userSchema.method('generateAuthToken', async function (remember = false) {
  // Generate an auth token for the user
  const user = this;
  const exp = remember ? 604800 : 86400;
  const token = jwt.sign({ id: user._id, password: user.password }, process.env.JWT_KEY, { expiresIn: exp });
  return token;
});

userSchema.static('authenticate', async (username: string, password: string) => {
  // Search for a user by username and password.
  const user = await User.findOne({ username });
  if (!user) return null;
  const isPasswordMatch = await bcrypt.compare(password, user.password);
  if (!isPasswordMatch) return true;

  return user;
});

userSchema.plugin(timestamps);
userSchema.plugin(require('mongoose-bcrypt'));

export const User: UserModel = model<UserInstance, UserModel>('User', userSchema);

export default User;
