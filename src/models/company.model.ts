import { model, Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import _ from 'lodash';

import { CompanyInstance, CompanyModel } from '../interfaces';

const ObjectId = Schema.Types.ObjectId;

export const companySchema = new Schema({
  name: {
    type: String,
    required: 'Nome da empresa é obrigatório',
  },
  company_pic: {
    type: String,
    trim: true,
    default: null,
  },
  phone: {
    type: Number,
  },
  document: {
    type: String,
    unique: true,
    required: 'CPF é obrigatório',
  },
  email: {
    type: String,
    required: 'Email é obrigatório',
    trim: true,
    lowercase: true,
    unique: true,
  },
  address: {
    address: {
      type: String,
      default: null,
    },
    number: {
      type: String,
      default: null,
    },
    complement: {
      type: String,
      default: null,
    },
    neighborhood: {
      type: String,
      default: null,
    },
    city: {
      type: String,
      default: null,
    },
    state: {
      type: String,
      default: null,
    },
    zipcode: {
      type: String,
      default: null,
    },
    country: {
      type: String,
      default: 'Brasil',
    },
  },
  active: {
    type: Boolean,
    default: true,
  }
});

companySchema.plugin(timestamps);
companySchema.plugin(require('mongoose-bcrypt'));

export const Company: CompanyModel = model<CompanyInstance, CompanyModel>('Company', companySchema);

export default Company;
