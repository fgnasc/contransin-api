import { model, Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import _ from 'lodash';

import { PermissionInstance, PermissionModel } from '../interfaces';

export const permissionSchema = new Schema({
    name: {
        type: String,
        required: 'Nome da permissão é obrigatório'
    },
    label: {
        type: String,
        required: "Label da permissão é obrigatório"
    }
})

permissionSchema.plugin(timestamps);
permissionSchema.plugin(require('mongoose-bcrypt'));

export const Permission: PermissionModel = model<PermissionInstance, PermissionModel>('Permission', permissionSchema);

export default Permission;
