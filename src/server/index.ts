import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express, { Application } from 'express';
import http from 'http';
import os from 'os';
import path from 'path';

import { errorHandler } from './middlewares';
// import { cronService } from '../services';

// import formidableMiddleware from 'express-formidable';

const app = express();

export default class ExpressServer {
  constructor() {
    const root = path.normalize(`${__dirname}/../..`);
    const corsOptions = {
      exposedHeaders: ['x-access-token', 'Authorization'],
      origin: '*',
      methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    };
    app.set('appPath', `${root}client`);
    app.use(cors(corsOptions));
    // app.use(requirePayment());
    app.use(bodyParser.json({ limit: process.env.REQUEST_LIMIT || '1024kb' }));
    app.use(bodyParser.urlencoded({ extended: true, limit: process.env.REQUEST_LIMIT || '1024kb' }));
    app.use(cookieParser(process.env.SESSION_SECRET));
    const publicFolder = `${path.normalize(`${__dirname}/..`)}/public`;
    app.use('/public', express.static(publicFolder));
  }

  router(routes: (app: Application) => void): ExpressServer {
    routes(app);
    app.use(errorHandler)
    return this;
  } 

  listen(p: string | number = process.env.PORT): Application {
    const welcome = port => () =>
      console.log(`up and running in ${process.env.NODE_ENV || 'development'} @: ${os.hostname()} on port: ${port}}`);
    http.createServer(app).listen(p, welcome(p));
    // cronService();
    return app;
  }
}
