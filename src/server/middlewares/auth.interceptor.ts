import bcrypt from 'bcryptjs';
import { Request, Response, NextFunction } from '../../config';
import { verify } from 'jsonwebtoken';
import { httpErrors } from '../../exceptions';
import { User } from '../../models';
import { userFromToken } from '../../helpers';

const HTTP_NOT_AUTHORIZED = { auth: false, error: httpErrors(401) },
  HTTP_FORBIDDEN = { auth: false, error: httpErrors(403) };

export const authenticateToken = async (token: string) => {
  try {
    let id: string;
    let password: string;
    try {
      const data = verify(token, process.env.JWT_KEY);
      if (data) {
        id = data.id;
        password = data.password;
      }
    } catch (err) {
      return null
    }

    // const user = await User.findOne({ _id: id, 'tokens.token': token });
    const user = await User.findOne({ _id: id, password });

    if (!user) return null;
    return user;
  } catch (error) {
    return null;
  }
};

export const authorize = (...allowed: string[]) => {
  const isAllowed = profile => allowed.indexOf(profile) > -1 || allowed.indexOf('all') > -1;

  return async (req: Request, res: Response, next: NextFunction) => {
    if (req.header('Authorization')) {
      const token = req.header('Authorization').replace('Bearer ', '');

      if (!token) {
        console.log(`Token not found`);
        res.status(401).json(HTTP_NOT_AUTHORIZED);
        next();
        return;
      }
      const user = await authenticateToken(token);      
      if (!user) {
        console.log(`User not found`);
        res.status(401).json(HTTP_NOT_AUTHORIZED);
        next();
        return;
      }
      
      // if (!(user && isAllowed(user.profile))) {
      //   console.log(`User not allowed to access this route`);
      //   res.status(403).json(HTTP_FORBIDDEN);
      // }

      req.user = user;
      next();
    } else {
      res.status(401).json(HTTP_NOT_AUTHORIZED);
      next();
    }
  };
};

export default authorize;
