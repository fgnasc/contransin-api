import express from 'express';

import { AuthenticationController } from '../../controllers';
import { authorize } from '../middlewares';

const Controller = new AuthenticationController();

export default express
  .Router()
  .post('/login', Controller.login)
  // .post('/logout', authorize('all'), Controller.logout)
  .post('/logout/all', authorize('all'), Controller.logoutAll);
