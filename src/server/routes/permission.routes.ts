import express from 'express';

import { PermissionController } from '../../controllers';
import { authorize } from '../middlewares';

const Controller = new PermissionController();

export default express
  .Router()
  .post('/', Controller.create)
  .get('/:_id', authorize(), Controller.findById)
  .patch('/:_id', authorize(), Controller.update)