import express from 'express';

import { PermissionsGroupController } from '../../controllers';
import { authorize } from '../middlewares';

const Controller = new PermissionsGroupController();

export default express
  .Router()
  .get('/:_id', Controller.findById)