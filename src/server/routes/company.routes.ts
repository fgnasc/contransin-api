import express from 'express';

import { CompanyController } from '../../controllers';
import { authorize } from '../middlewares';

const Controller = new CompanyController();

export default express
  .Router()
  .post('/', Controller.create)
  .get('/:_id', authorize(), Controller.findById)