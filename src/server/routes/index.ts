import { Application } from 'express';
import express from 'express';
import authRouter from './authentication.routes';
import usersRouter from './users.routes';
import CompanyRouter from './company.routes';
import PermissionRouter from './permission.routes';
import PermissionsGroupRouter from './permissions_group.routes';
import GroupRouter from './group.routes';
const apiBasePath = '/api/v1';

export default function routes(app: Application): void {
  app.use(`${apiBasePath}/`, authRouter);
  app.use(`${apiBasePath}/users`, usersRouter);
  app.use(`${apiBasePath}/company`, CompanyRouter);
  app.use(`${apiBasePath}/permission`, PermissionRouter);
  app.use(`${apiBasePath}/permission-group`, PermissionsGroupRouter);
  app.use(`${apiBasePath}/group`, GroupRouter);

  app.use(express.static('public'));

}
