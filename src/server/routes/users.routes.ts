import express from 'express';

import { UserController } from '../../controllers';
import { authorize } from '../middlewares';
import { fileUploaderFactory } from '../../config';

const fileUploader = fileUploaderFactory({ folderName: 'profile_pic' });

const Controller = new UserController();

export default express
  .Router()
  .post('/', Controller.create)
  .get('/', authorize(), Controller.getUser)
  .get('/:_id', authorize('patient', 'user', 'admin', 'system'), Controller.findById)
  .put('/:_id', authorize('patient', 'user', 'admin', 'system'), Controller.update)
  .patch('/:_id/profile-pic', authorize('patient', 'user', 'admin', 'system'), fileUploader.single('profilePic'), Controller.updateProfilePic)
  .patch('/change-password', authorize('patient', 'user', 'admin', 'system'), Controller.changePassword)
  .get('/:email/recuperar', Controller.recoverPassword)
  