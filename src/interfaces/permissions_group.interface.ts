import { Moment } from 'moment';
import { Document, Model } from 'mongoose';
import { Permission } from './';

export type PermissionsGroupInstance = PermissionsGroup;

export type PermissionsGroupModel = Model<PermissionsGroupInstance>;

export interface PermissionItem {
  permission: string | Permission;
  active: boolean;
}

export interface PermissionsGroup extends Document{
  name: string;
  company: string;
  permissions: PermissionItem[];
  updatedAt: Date | Moment;
  createdAt: Date | Moment;
}

export default PermissionsGroup;
