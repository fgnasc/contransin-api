import { Document, Model } from 'mongoose';
import Address from './address.interface';
import { Moment } from 'moment';
import PermissionsGroup from './permissions_group.interface';
import Company from './company.interface';

export interface UserInstance extends User {
  // Methods
  generateAuthToken(remember: boolean): string;
}

export interface UserModel extends Model<UserInstance> {
  // Statics
  authenticate(username: string, password: string): UserInstance;
}

export interface User extends Document {
  username: string;
  password: string;
  profile_pic?: string;
  name: string;
  email: string;
  gender: 'MASC' | 'FEM' | 'OTHER';
  company: string | Company;
  firstLogin?: boolean;
  birth?: string | Date | Moment;
  group?: PermissionsGroup | string;
  phone?: string;
  document?: string;
  address?: Address;
  active: boolean;
  updatedAt?: Date | Moment;
  createdAt?: Date | Moment;
}
export default User;