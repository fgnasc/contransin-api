import { Moment } from 'moment';
import { Document, Model } from 'mongoose';
import Address from './address.interface';
import PermissionsGroup from './permissions_group.interface';

export type CompanyInstance = Company;

export type CompanyModel = Model<CompanyInstance>;

export interface Company extends Document {
  name: string;
  company_pic: string;
  phone: string;
  document: string;
  email: string;
  address: Address;
  active: boolean;
  permissions_group: PermissionsGroup[];
  updatedAt: Date | Moment;
  createdAt: Date | Moment;
}

export default Company;
