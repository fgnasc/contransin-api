import { Document, Model } from 'mongoose';
import PermissionsGroup from './permissions_group.interface';

export type GroupInstance = Group;

export type GroupModel = Model<GroupInstance>;

export interface Group extends Document {
  name: string;
  permissions_group: string | PermissionsGroup;

}

export default Group;
