import { Moment } from "moment";
import { Document, Model } from "mongoose";

export type PermissionInstance = Permission;

export type PermissionModel = Model<PermissionInstance>;

export interface Permission extends Document{
  name: string;
  label: string;
  updatedAt: Date | Moment;
  createdAt: Date | Moment;
}

export default Permission;
