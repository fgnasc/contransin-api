import { Request, Response, NextFunction } from '../config';
import { HttpException } from '../exceptions';
import { AuthenticationService } from '../services';

const Service = new AuthenticationService();

export class AuthenticationController {
  login = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { username, password, remember } = req.body;
    try {
      const response = await Service.login(username, password, remember);
      res.status(200).json(response);
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

  // logout = async (req: Request, res: Response): Promise<void> => {
  //   const response = await Service.logout(req.body.id, req.body.token);
  //   res.status(200).json(response);
  // };

  logoutAll = async (req: Request, res: Response): Promise<void> => {
    const response = Service.logoutAll(req.body.id);
    res.status(200).json(response);
  };
}

export default new AuthenticationController();