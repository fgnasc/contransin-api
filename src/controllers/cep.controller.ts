import { Request, Response, NextFunction } from '../config';
import http from 'http';
import https from 'https';
import { HttpException } from '../exceptions';

export class CepController {
  public url = 'http://cep.la/';
  public searchZipCode = (req: Request, res: Response, next: NextFunction): void => {
    try {
      const options = {
        headers: {
          Accept: 'application/json',
        },
      };
      const lib = this.url.startsWith('https') ? https : http;
      const request = lib.get(this.url + req.params.cep, options, response => {
        let data = '';

        response.on('data', chunk => {
          data += chunk;
        });
        response.on('end', () => {
          const parsedData = JSON.parse(data);
          if (!parsedData || parsedData instanceof Array) {
            res.statusCode = 400;
            res.json({
              message: "CEP inválido"
            });
            next();
          } else {
            res.json(parsedData);
          }
        });
      });
      request.on('error', err => {
        throw err;
      });
    } catch (error) {
      next(new HttpException(400, error));
    }
  };
}

export default CepController;