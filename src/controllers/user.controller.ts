import { Request, Response, NextFunction } from '../config';
import { HttpException } from '../exceptions';
import { UPLOADS_FOLDER } from '../config';
import { decodeToken } from '../helpers';
import { UserService } from '../services';
export class UserController {
  constructor() {}

  create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      if (!req.body) {
        next(new HttpException(400, 'Falha ao receber dados do usuário'));
      }
      const user = await UserService.create(req.body);
      res.status(201).json(user);
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

  update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      if (req.body) {
        const instance = await UserService.update(req.body);
        res.status(200).json(instance);
      }
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

  updateProfilePic = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { filename, size } = (req as any).file;
      const path = `${UPLOADS_FOLDER}/profile_pic/${filename}`;
      const response = await UserService.updateProfilePicById(req.params._id, path);
      res.status(200).json({ filename, size, profilePicUrl: path });
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

  changePassword = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      if (req.body.password.length <= 6) {
        next(new HttpException(415, 'Senha muito curta!'));
        return;
      }
      const response = await UserService.changePassword(req.user._id, req.body);
      res.status(200).json(response);
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

  findById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const instance = await UserService.findById({ _id: req.params._id });
      res.status(200).json(instance);
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

  getUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const instance = await UserService.findById({ _id: req.user._id });
      console.log(req.user);
      
      res.status(200).json(instance);
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

  recoverPassword = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const response = await UserService.recover(req.params.email);
      res.status(200).json(response);
    } catch (error) {
      next(new HttpException(400, error));
    }
  };
}

export default new UserController();
