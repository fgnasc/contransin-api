import { Request, Response, NextFunction } from '../config';
import { HttpException } from '../exceptions';
import { UPLOADS_FOLDER } from '../config';
import { GroupService } from '../services';
export class GroupController {
  constructor() {}

  create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      if (!req.body) {
        next(new HttpException(400, 'Falha ao receber dados do usuário'));
      }
      const group = await GroupService.create(req.body);
      res.status(201).json(group);
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

  update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      if (req.body) {
        const instance = await GroupService.update(req.body);
        res.status(200).json(instance);
      }
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

  findById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const instance = await GroupService.find({ _id: req.params._id });
      res.status(200).json(instance);
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

}

export default new GroupController();