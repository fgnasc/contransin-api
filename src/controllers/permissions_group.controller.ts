import { Request, Response, NextFunction } from '../config';
import { HttpException } from '../exceptions';
import { PermissionService } from '../services';
export class PermissionsGroupController {
  constructor() {}
  create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      if (!req.body) {
        next(new HttpException(400, 'Falha ao receber dados do usuário'));
      }
      const company = await PermissionService.create(req.body);
      res.status(201).json(company);
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

  update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      if (req.body) {
        const instance = await PermissionService.update(req.body);
        res.status(200).json(instance);
      }
    } catch (error) {
      next(new HttpException(400, error));
    }
  };
  findById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const instance = await PermissionService.find({ _id: req.params._id });
      res.status(200).json(instance);
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

}
export default new PermissionsGroupController();
