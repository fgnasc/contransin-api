import { Request, Response, NextFunction } from '../config';
import { HttpException } from '../exceptions';
import { UPLOADS_FOLDER } from '../config';
import { CompanyService } from '../services';
export class CompanyController {
  constructor() {}

  create = async (req: Request, res: Response, next: NextFunction) => {
    try {
      if (!req.body) {
        next(new HttpException(400, 'Falha ao receber dados do usuário'));
      }
      const company = await CompanyService.create(req.body);
      res.status(201).json(company);
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

  update = async (req: Request, res: Response, next: NextFunction) => {
    try {
      if (req.body) {
        const instance = await CompanyService.update(req.body);
        res.status(200).json(instance);
      }
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

  updateProfilePic = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { filename, size } = (req as any).file;
      const path = `${UPLOADS_FOLDER}/profile_pic/${filename}`;
      const response = await CompanyService.updateProfilePicById(req.params._id, path);
      res.status(200).json({ filename, size, profilePicUrl: path });
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

  findById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const instance = await CompanyService.find({ _id: req.params._id });
      res.status(200).json(instance);
    } catch (error) {
      next(new HttpException(400, error));
    }
  };

}

export default new CompanyController();