import mongoose from 'mongoose';

const connection = mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

mongoose.set('useCreateIndex', true);

connection.then(db => db).catch(err => console.log(err));

export default connection;
