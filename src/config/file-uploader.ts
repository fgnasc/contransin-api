import multer from 'multer';
import path from 'path';

// export const
export const UPLOADS_FOLDER = '/images';
export const FULL_UPLOADS_FOLDER = path.resolve(__dirname, `../../public${UPLOADS_FOLDER}`);

export function fileUploaderFactory(config: { [key: string]: any } = {}) {
  if (!config.folderName) {
    config.folderName = 'tmp';
  }

  const defaultConfig = {
    destination: `${FULL_UPLOADS_FOLDER}/${config.folderName}`,
    filename: (req, file, callback) => {
      const fileName = `${Date.now()}-${file.originalname}`;
      return callback(null, fileName);
    },
  };

  if (typeof config.fileFilter === 'string') {
    config.fileFilter = [config.fileFilter];
  }
  if (config.fileFilter instanceof Array && config.fileFilter.every(x => typeof x === 'string')) {
    config.fileFilter = (req, file, callback) => {
      const filterAllows = config.fileFilter.some(t => file.mimetype.includes.includes(t));
      callback(null, filterAllows);
    };
  }

  const storage = multer.diskStorage({ ...defaultConfig, config });

  return multer({ storage });
}
