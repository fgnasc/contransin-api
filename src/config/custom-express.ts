import express, { Request as ExpressRequest, Response as ExpressResponse, NextFunction as ExpressNextFunction } from 'express';
import consign from 'consign';
import bodyParser from 'body-parser';
import { UserInstance } from '../interfaces';

module.exports = () => {
  const app = express();

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  consign().include('controllers').into(app);

  return app;
};

export interface Request extends ExpressRequest {
  user: UserInstance;
  isFree: boolean;
}
export type Response = ExpressResponse;
export type NextFunction = ExpressNextFunction;
