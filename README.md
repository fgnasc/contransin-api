# cinesiopro-api

API for CinesioPro app

## Quick Start

Get started developing...

```shell
# install deps
yarn install

# run in development mode
yarn dev
```

---

## Install Dependencies

Install all package dependencies (one time operation)

```shell
yarn install
```

## Run It
#### Run in *development* mode:
Runs the application is development mode. Should not be used in production

```shell
yarn dev
```

#### Run in *production* mode:

Compiles the application and starts it in production production mode.

```shell
yarn build
yarn start
```

## Try It
* Open the GraphQL Playground at [http://localhost:3000](http://localhost:3000)

## Warning
* This repo uses Husky to run git hooks, it will lint your code on pre-commit, only push to origin if there are no errors