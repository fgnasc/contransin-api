import '../../../src/config/env';
import { expect } from 'chai';
import { authenticateToken } from '../../../src/server/middlewares';

describe('Check token', () => {
  it("shouldn't be null", () => {
    authenticateToken(
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlYzc3NTM1MjA0YjE4NmIwMTljOWVkMCIsImlhdCI6MTU5MDQ0NTI4MywiZXhwIjoxNTkwNTMxNjgzfQ.QgpwrrTK0dAg9wCkpGHp5_bSpoVhUwbDZhWH1wOFEaw'
    ).then(result => expect(result).to.not.be.null);
  });
});
