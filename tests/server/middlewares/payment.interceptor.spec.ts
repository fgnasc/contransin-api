import { expect } from 'chai';
import { checkRoute } from '../../../src/server/middlewares';

describe('Check free routes', function () {
  describe('login', () => {
    it('POST should be true', function () {
      const result = checkRoute('POST', '/api/v1/login');
      expect(result).equal(true);
    });
    it('GET should be false', function () {
      const result = checkRoute('GET', '/api/v1/login');
      expect(result).equal(false);
    });
  });
  describe('logout', () => {
    it('POST should be true', function () {
      const result = checkRoute('POST', '/api/v1/logout');
      expect(result).equal(true);
    });
    it('GET should be false', function () {
      const result = checkRoute('GET', '/api/v1/logout');
      expect(result).equal(false);
    });
  });
  describe('clean sessions', () => {
    it('POST should be true', function () {
      const result = checkRoute('POST', '/api/v1/logout/all');
      expect(result).equal(true);
    });
    it('GET should be false', function () {
      const result = checkRoute('GET', '/api/v1/logout/all');
      expect(result).equal(false);
    });
  });
  describe('update profile', () => {
    it('POST should be true', function () {
      const result = checkRoute('POST', '/api/v1/users/5ec7759bdd47d26c0362b380');
      expect(result).equal(true);
    });
    it('GET should be true', function () {
      const result = checkRoute('GET', '/api/v1/users/5ec7759bdd47d26c0362b380');
      expect(result).equal(true);
    });
    it('POST with invalid ObjectId should be false', function () {
      const result = checkRoute('POST', '/api/v1/users/nomedousuariocom24caract');
      expect(result).equal(false);
    });
  });
});
